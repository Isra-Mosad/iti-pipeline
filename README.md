# Creating CI/ CD Pipeline Using Gitlab CI



![alt text](https://gitlab.com/devops7475312/iti-pipeline/-/raw/main/images/owsap.png)

---

## Theory
- ### Automated pipeline that runs on Gitlab Runner.
- ### Build a CI/CD pipeline to do the following
    - Deploy "Juice Shop" application, using the official docker image from [**‘bkimminich/juice-shop’**](https://hub.docker.com/r/bkimminich/juice-shop). 
    - Expose “Juice Shop” application inside the cluster using a service. 
    - “Juice Shop” application is exposed outside the cluster using NodePort Service.

![alt text](https://gitlab.com/devops7475312/iti-pipeline/-/raw/main/images/Architecture.png)

---

## Requirements
- #### **K8s cluster (one master & one node).**
  - #### **Centos 8 Stream** distribution for both Master and Worker
  - #### The lightweight Kubernetes K3S to build my own cluster. [Docs](https://docs.k3s.io)
- #### **GitLab Server.**
  - #### **Centos 8 Stream** distribution.
- #### **GitLab Runner installed locally on the GitLab server.**
 
 ---
 
## Install K8S Cluster using K3S
- ### Install Master
    - `curl -sfL https://get.k3s.io | sh -s - --disable traefik --write-kubeconfig-mode 644 --node-name k3s-master-01`
    - Configure the **.kube** directory because K3S doesn't create the configuration directory by default.
    ```
    export KUBECONFIG=~/.kube/config
    mkdir ~/.kube 2> /dev/null
    sudo k3s kubectl config view --raw > "$KUBECONFIG"
    chmod 600 "$KUBECONFIG"
    ```
- ### Install Worker
   - Grab token from the master node to be able to add worked nodes to it:
     `cat /var/lib/rancher/k3s/server/node-token`
  - Install k3s on the worker node and add it to the cluster:
     - `curl -sfL https://get.k3s.io | K3S_NODE_NAME=k3s-worker-01 K3S_URL=https://<Master_IP>:6443 K3S_TOKEN=<TOKEN> sh - `
     - > **Note:** <TOKEN> is the token we got from path `/var/lib/rancher/k3s/server/node-token` inside our master

- ### Validating
  - Go to your master machine and execute the command 
    `kubectl get nodes -o wide`

    Now the output should be as below (Except your own IPs) 
  ![alt text](https://gitlab.com/devops7475312/iti-pipeline/-/raw/main/images/validate_nodes.png)

---

## Stages
- ### Deploy Workload Stage.
- ### Testing Stage
![alt text](https://gitlab.com/devops7475312/iti-pipeline/-/raw/main/images/02_deploy_workload_stage.png)

### Deploy Workload Stage.
- This stage access the kubernetes master using ssh and applying our workload from the definition files in the same project (**deployment.yml, service-internal.yml, nodeport-service.ymy**)

### Testing Stage
- This stage displays the output of the actual Pods, Services and nodes attached to the kubernetes cluster.

## Verification
- ### Now, user can access the application using the NodeIP:NodePortIP by using:-
  - ##### curl from inside the master 
    ![alt text](https://gitlab.com/devops7475312/iti-pipeline/-/raw/main/images/curl_from_master_on_nodeport.png)

  - ##### Web Browser:- (https://NodeIp:NodePort)
    ![alt text](https://gitlab.com/devops7475312/iti-pipeline/-/raw/main/images/GUI_Testing.png)


### Thank you for taking the time to read this README file. If you have any questions or feedback, please don't hesitate to contact me at [m.salah.azim@gmail.com](mailto:m.salah.azim@gmail.com).  


